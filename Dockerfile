# Pre-build
FROM alpine-base:latest
MAINTAINER Alves Oliveira <luiz.c.de.oliveira@accenture.com>

USER root

#ENV HTTP_PROXY "http://proxy.redecorp.br:8080"
#ENV HTTPS_PROXY "http://proxy.redecorp.br:8080"

ADD files/HYBRISCOMM5700P_28-70001174.ZIP /
RUN apk update \
	 && apk add unzip \
	 && mkdir /hybris \
	 && unzip HYBRISCOMM5700P_28-70001174.ZIP 'hybris/*' \
	 && rm -rf HYBRISCOMM5700P_28-70001174.ZIP

WORKDIR /hybris/bin/platform
RUN . ./setantenv.sh \
 	 && echo | ant clean all

# Build finally IMG
FROM alpine-base:latest
MAINTAINER Alves Oliveira <luiz.c.de.oliveira@accenture.com>

#ENV HTTP_PROXY "http://proxy.redecorp.br:8080"
#ENV HTTPS_PROXY "http://proxy.redecorp.br:8080"

COPY --from=0 /hybris /

RUN apk update \
	 && apk add unzip rsync git openssh nodejs \
	 && npm config set strict-ssl false \
	 && npm install -g gulp

ENV JENKINS_HOME=/opt/jenkins_home

EXPOSE 22
